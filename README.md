Small script that monitor a tree (folder and optionally subfolders) - looking for files with spaces in name - and replace spaces to underscores, plus removes quotes, brackets, commas.

Without dependencies, without inofity.

On start it scans full directory and can rename files that already have spaces in names. This is just `find` with `sleep 1` with `mv` in a loop.

![video](/demo.webm)

UPDATE: I found that this is dangerous: when Firefox downloads a big file - and if it renamed during downloading - you will get empty file.